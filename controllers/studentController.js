import Student from "../models/studentModel.js";

// Get all students
export const getStudents = async (req, res) => {
  try {
    const students = await Student.find();
    res.json(students);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Get a student
export const getStudent = async (req, res) => {
  try {
    const student = await Student.findById(req.params.id);
    res.json(student);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

// Create a student
export const createStudent = async (req, res) => {
  const student = new Student({
    name: req.body.name,
    age: req.body.age,
    birthDay: req.body.birthDay,
  });

  try {
    const newStudent = await student.save();
    res.status(201).json(newStudent);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

// Update a student
export const updateStudent = async (req, res) => {
  try {
    const student = await Student.findById(req.params.id);
    student.name = req.body.name;
    student.age = req.body.age;
    student.birthDay = req.body.birthDay;

    const updatedStudent = await student.save();
    res.json(updatedStudent);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

// Delete a student
export const deleteStudent = async (req, res) => {
  try {
    await Student.findByIdAndDelete(req.params.id);
    res.json({ message: "Student deleted" });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};
