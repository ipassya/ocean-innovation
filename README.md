# ocean-innovation

## Getting started

Assuming you’ve already installed [Node.js](https://nodejs.org/en/download/package-manager) and [MongoDB Compass](https://www.mongodb.com/try/download/compass), create a directory to hold your application, and make that your working directory.

- Express 4.x requires Node.js 0.10 or higher.
- Express 5.x requires Node.js 18 or higher.

Then install dependencies:<br />
`$ cd ocean-innovation`<br />
`$ npm install`

On Git Bash, run the app with this command:<br />
`$ npm start`

Then, load http://localhost:5000/ in your browser to access the app.
