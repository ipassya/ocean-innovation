import mongoose from "mongoose";

const studentSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  age: {
    type: Number,
    required: true,
  },
  birthDay: {
    type: Date,
    required: true,
  },
});

export default mongoose.model("Students", studentSchema);
